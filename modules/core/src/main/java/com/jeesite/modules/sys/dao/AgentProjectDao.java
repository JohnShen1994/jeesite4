/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.sys.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.sys.entity.AgentProject;


/**
 * @author apple
 */
@MyBatisDao
public interface AgentProjectDao extends CrudDao<AgentProject> {

}