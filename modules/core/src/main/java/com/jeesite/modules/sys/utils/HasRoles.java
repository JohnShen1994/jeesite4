package com.jeesite.modules.sys.utils;

import com.jeesite.common.lang.ObjectUtils;
import com.jeesite.common.lang.StringUtils;
import org.beetl.core.Context;
import org.beetl.core.Function;

/**
 * beet页面判断是否具有某个角色
 */
public class HasRoles implements Function {

    public HasRoles() {
    }

    @Override
    public Object call(Object[] paras, Context context) {
        if(!StringUtils.isNotBlank(ObjectUtils.toString(paras[0]))){
            return false;
        }
        for(String role : ObjectUtils.toString(paras[0]).split(",")){
            if(UserUtils.getSubject().hasRole(role)){
                return true;
            }
        }
        return false;
    }
}
