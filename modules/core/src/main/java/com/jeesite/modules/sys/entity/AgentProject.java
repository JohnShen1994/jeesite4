package com.jeesite.modules.sys.entity;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;

/**
 * @author apple
 */
@Table(name="vip_agent_project", alias="a", columns={
        @Column(name="user_id", label="员工id"),
        @Column(name="project_id", label="项目id")
    }, orderBy="a.project_id"
)
public class AgentProject extends DataEntity<AgentProject> {

    private static final long serialVersionUID = 1L;

    /**user_id 即代理id**/
    private String userId;

    /**项目Id**/
    private String projectId;

    public AgentProject() {
        this(null, null);
    }

    public AgentProject(String userId, String projectId){
        this.userId = userId;
        this.projectId = projectId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
