function formatNumber(num){
    if(num =='' || num == undefined){
        return '0.000';
    }
    if(isNaN(parseFloat(num))){
        return '0.000';
    }
    num = parseFloat(num);
    if(num<0){return  '<span style="color:red">'+num.toFixed(3)+'</span>';}else{return	num.toFixed(3);}
}

function formatNumberWithoutHtml(num){
    if(num =='' || num == undefined ){
        return '0.000';
    }
    if(isNaN(parseFloat(num))){
        return '0.000';
    }
    return  parseInt(num).num.toFixed(3);
}

function formatText(text){
    return (text == "" || text == undefined) ? "--" : text;
}

function preTabCallback1(){
    console.log(js.getCurrentTabPage(preTabCallback()));
}
function preTabCallback(){
   // alert("preTabCallback");
}
//返回上一个Tab并关闭当前Tab
function goBackRreTab() {
    js.getPrevTabPage(preTabCallback(), true);
}