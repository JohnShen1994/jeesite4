/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.vip.entity.Project;

import java.util.List;
import java.util.Map;

/**
 * projectDAO接口
 * @author xtp
 * @version 2018-08-22
 */
@MyBatisDao
public interface ProjectDao extends CrudDao<Project> {

    List<Project> findAgentProject(Map map);
}