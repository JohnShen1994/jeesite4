package com.jeesite.modules.vip.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mac
 * @date 16/4/21
 */
public enum VipUserTypeEnum {
    /**
     *vip用户类型（1高级代理、2普通代理、3财务、4系统管理员、5vip代理、6审核员 、7只读角色）
     */
    HEIGHT_AGENT(1, "1"),
    NORMAL_AGENT(2, "2"),
    FINANCE(3, "3"),
    ADMIN(4,"4"),
    VIP_AGENT(5,"5"),
    CHECK_USER(6,"6"),
    READONLY_USER(7,"7");

    private Integer key;
    private String value;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private VipUserTypeEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getValue(int index) {
        for (VipUserTypeEnum item : VipUserTypeEnum.values()) {
            if (item.getKey() == index) {
                return item.value;
            }
        }
        return null;
    }

    // 把 enum 转为 list 使用
    public static List<Map> getList() {
        List<Map> list = new ArrayList();
        Map map = null;
        for (VipUserTypeEnum item : VipUserTypeEnum.values()) {
            map = new HashMap();
            map.put("key", item.getKey());
            map.put("value", item.getValue());

            list.add(map);
        }
        return list;
    }
}
