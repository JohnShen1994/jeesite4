/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.vip.entity.VipUser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * projectDAO接口
 * @author xtp
 * @version 2018-08-22
 */
@MyBatisDao
public interface VipUserDao extends CrudDao<VipUser> {

    List<HashMap<String,Object>> findAgentList(Map<String,Object> map);
}