package com.jeesite.modules.vip.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mac
 * @date 16/4/21
 */
public enum RecordTypeEnum {
    /**
     *记录类型（1出货、2入货、3结账、4充值）
     */
    OUT(1, "1"),
    IN(2, "2"),
    CHECKOUT(3, "3"),
    RECHARGE(4, "4");

    private Integer key;
    private String value;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private RecordTypeEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getValue(int index) {
        for (RecordTypeEnum item : RecordTypeEnum.values()) {
            if (item.getKey() == index) {
                return item.value;
            }
        }
        return null;
    }

    // 把 enum 转为 list 使用
    public static List<Map> getList() {
        List<Map> list = new ArrayList();
        Map map = null;
        for (RecordTypeEnum item : RecordTypeEnum.values()) {
            map = new HashMap();
            map.put("key", item.getKey());
            map.put("value", item.getValue());

            list.add(map);
        }
        return list;
    }

    /**
     * 获取type对应的名称
     * @param type 记录类型（1出货、2入货、3结账、4充值）
     * @return name
     */
    public static String getRecordTypeName(String type){
      switch (type){
          case "1":
              return "出货";
          case "2":
              return "入货";
          case "3":
              return "结账";
          case "4":
              return "充值";
          default:
              return "";
      }
    }
}
