/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.vip.entity.Project;
import com.jeesite.modules.vip.service.ProjectService;

/**
 * projectController
 * @author xtp
 * @version 2018-08-22
 */
@Controller
@RequestMapping(value = "${adminPath}/vip/project")
public class ProjectController extends BaseController {

	@Autowired
	private ProjectService projectService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Project get(String id, boolean isNewRecord) {
		return projectService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("vip:project:view")
	@RequestMapping(value = {"list", ""})
	public String list(Project project, Model model) {
		model.addAttribute("project", project);
		return "modules/vip/projectList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("vip:project:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Project> listData(Project project, HttpServletRequest request, HttpServletResponse response) {
		Page<Project> page = projectService.findPage(new Page<Project>(request, response), project); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("vip:project:view")
	@RequestMapping(value = "form")
	public String form(Project project, Model model) {
		model.addAttribute("project", project);
		return "modules/vip/projectForm";
	}

	/**
	 * 保存project
	 */
	@RequiresPermissions("vip:project:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Project project) {
		projectService.save(project);
		return renderResult(Global.TRUE, text("保存project成功！"));
	}
	
	/**
	 * 停用project
	 */
	@RequiresPermissions("vip:project:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(Project project) {
		project.setStatus(Project.STATUS_DISABLE);
		projectService.updateStatus(project);
		return renderResult(Global.TRUE, text("停用project成功"));
	}
	
	/**
	 * 启用project
	 */
	@RequiresPermissions("vip:project:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(Project project) {
		project.setStatus(Project.STATUS_NORMAL);
		projectService.updateStatus(project);
		return renderResult(Global.TRUE, text("启用project成功"));
	}
	
	/**
	 * 删除project
	 */
	@RequiresPermissions("vip:project:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Project project) {
		projectService.delete(project);
		return renderResult(Global.TRUE, text("删除project成功！"));
	}
	
}