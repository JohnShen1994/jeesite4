package com.jeesite.modules.vip.utils;

import com.jeesite.modules.sys.utils.DictUtils;
import com.jeesite.modules.vip.entity.Record;

import java.util.List;

/**
 * @author apple
 */
public class RecordToExcelUtils {

    public static List<Record> recordToModel(List<Record> records){
        for(Record record:records){
            record.setType(DictUtils.getDictLabel("vip_record_type",record.getType(),"--"));
            record.setReviewerState(DictUtils.getDictLabel("vip_review_state",record.getReviewerState(),"--"));
        }
        return records;
    }
}
