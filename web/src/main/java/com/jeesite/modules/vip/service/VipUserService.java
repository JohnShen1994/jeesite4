package com.jeesite.modules.vip.service;

import com.jeesite.common.service.CrudService;
import com.jeesite.modules.vip.dao.VipUserDao;
import com.jeesite.modules.vip.entity.VipUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author apple
 */
@Service
@Transactional(readOnly=true)
public class VipUserService extends CrudService<VipUserDao, VipUser> {

    /**
     * 查找代理商用户
     * @return agentList
     */
    public List<HashMap<String,Object>> findAgentList(String userCode){
        Map<String,Object> map = new HashMap<>();
        map.put("userCode",userCode);
        return dao.findAgentList(map);
    }
}
