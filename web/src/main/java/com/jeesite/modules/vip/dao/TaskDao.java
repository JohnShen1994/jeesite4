/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.dao;


import com.jeesite.common.mybatis.annotation.MyBatisDao;

/**
 *
 */
@MyBatisDao
public interface TaskDao {

    /**
     * 刪除日志
     */
    void deleteLogs();

    /**
     * 删除记录
     */
    void deleteRecords();
}