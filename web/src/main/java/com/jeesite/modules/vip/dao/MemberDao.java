/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.entity.Page;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.sys.entity.User;
import com.jeesite.modules.vip.entity.Member;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * memberDAO接口
 * @author xtp
 * @version 2018-08-22
 */
@MyBatisDao
public interface MemberDao extends CrudDao<Member> {

    List findAgentRecordPage(Map map);

    int findAgentRecordPageCount(Map map);

    List findMemberRecordPage(Map map);

    int findMemberRecordPageCount(Map map);

    HashMap<String,Object> countMemberListData(HashMap<String,Object> map);
}