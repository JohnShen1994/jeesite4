/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.entity;

import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.modules.sys.entity.User;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * memberEntity
 * @author xtp
 * @version 2018-08-22
 */
@Table(name="vip_member", alias="a", columns={
		@Column(name="id", attrName="id", label="会员id", isPK=true),
		@Column(name="name", attrName="name", label="会员姓名", queryType=QueryType.LIKE),
		@Column(name="mobile", attrName="mobile", label="手机号", queryType=QueryType.LIKE),
		@Column(name="wechat", attrName="wechat", label="微信号", queryType=QueryType.LIKE),
		@Column(name="agent_id", attrName="agentId", label="代理id",queryType=QueryType.LIKE),
		@Column(name="balance", attrName="balance", label="余额"),
		@Column(name="points", attrName="points", label="积分"),
		@Column(name="checkout_money", attrName="checkoutMoney", label="结账金额"),
		@Column(name="recharge_total", attrName="rechargeTotal", label="总充值"),
		@Column(name="pay_total", attrName="payTotal", label="总支出"),
		@Column(name="in_total", attrName="inTotal", label="总入货"),
		@Column(name="out_total", attrName="outTotal", label="总出货"),
		@Column(includeEntity=DataEntity.class),
},
		joinTable = {
				@JoinTable(type = JoinTable.Type.LEFT_JOIN,entity = User.class,alias = "u",
						on="u.user_code = a.agent_id",columns = {@Column(includeEntity = User.class)})
		}, orderBy="SIGN(a.balance) DESC , a.update_date DESC"
)
public class Member extends DataEntity<Member> {

	private static final long serialVersionUID = 1L;
	/**会员姓名**/
	private String name;
	/**手机号**/
	private String mobile;
	/**微信号**/
	private String wechat;
	/**代理id**/
	private String agentId;
	/**余额**/
	private Double balance;
	/**积分**/
	private Double points;
	/**结账金额**/
	private Double checkoutMoney;
	/**总充值**/
	private Double rechargeTotal;
	/**总支出**/
	private Double payTotal;
	/**总入货**/
	private Double inTotal;
	/**总出货**/
	private Double outTotal;
	private User user;

	public Member() {
		this(null);
	}

	public Member(String id){
		super(id);
	}

	@NotBlank(message="会员姓名不能为空")
	@Length(min=0, max=32, message="会员姓名长度不能超过 32 个字符")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@NotBlank(message="手机号不能为空")
	@Length(min=0, max=20, message="手机号长度不能超过 20 个字符")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Length(min=0, max=20, message="微信号长度不能超过 20 个字符")
	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	@NotBlank(message="代理商不能为空")
	@Length(min=0, max=64, message="代理id长度不能超过 64 个字符")
	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Double getPoints() {
		return points;
	}

	public void setPoints(Double points) {
		this.points = points;
	}

	public Double getCheckoutMoney() {
		return checkoutMoney;
	}

	public void setCheckoutMoney(Double checkoutMoney) {
		this.checkoutMoney = checkoutMoney;
	}

	public Double getRechargeTotal() {
		return rechargeTotal;
	}

	public void setRechargeTotal(Double rechargeTotal) {
		this.rechargeTotal = rechargeTotal;
	}

	public Double getPayTotal() {
		return payTotal;
	}

	public void setPayTotal(Double payTotal) {
		this.payTotal = payTotal;
	}

	public Double getInTotal() {
		return inTotal;
	}

	public void setInTotal(Double inTotal) {
		this.inTotal = inTotal;
	}

	public Double getOutTotal() {
		return outTotal;
	}

	public void setOutTotal(Double outTotal) {
		this.outTotal = outTotal;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}