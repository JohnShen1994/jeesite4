package com.jeesite.modules.vip.utils;

import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import com.jeesite.common.config.Global;
import org.json.JSONException;

import java.io.IOException;

/**
 * 腾讯云短信服务
 */
public class SMSUtils {

    private final static int APP_ID = Integer.parseInt(Global.getProperty("SMS.APPID"));
    private final static String APP_KEY = Global.getProperty("SMS.APPKEY");
    private final static String SMS_SIGN = Global.getProperty("SMS.SIGN");
    public final static int SMS_TEMPLATE_ID_1 = Integer.parseInt(Global.getProperty("SMS.TEMPLATE_ID_1"));
    public final static int SMS_TEMPLATE_ID_2 = Integer.parseInt(Global.getProperty("SMS.TEMPLATE_ID_2"));
    public final static int SMS_TEMPLATE_ID_3 = Integer.parseInt(Global.getProperty("SMS.TEMPLATE_ID_3"));



    public static void sendingNotice(int templateId ,String phoneNumber ,String ...params){
        try {
            params = params == null ? new String[]{} : params;
            SmsSingleSender ssender = new SmsSingleSender(APP_ID, APP_KEY);
            SmsSingleSenderResult result = ssender.sendWithParam("86", phoneNumber,
                    templateId, params, SMS_SIGN, "", "");
            System.out.println(result);
        } catch (HTTPException e) {
            // HTTP响应码错误
            e.printStackTrace();
        } catch (JSONException e) {
            // json解析错误
            e.printStackTrace();
        } catch (IOException e) {
            // 网络IO错误
            e.printStackTrace();
        }
    }

    /**
     * 根据操作类型获取短信模版（坑爹的腾讯云短信）
     * 记录类型（1出货、2入货、3结账、4充值、5会员创建通知）
     * @param type 操作类型
     * @return templateId
     */
    public static int getTemplateId(String type){
        switch (type){
            case "1":
                return 274628;
            case "2":
                return 274631;
            case "3":
                return 274664;
            case "4":
                return 274661;
            case "5":
                return 274752;
            default:
                return 0;
        }
    }

    /**
     * 根据操作类型获取短信模版发送给会员
     * 记录类型（1出货、2入货、3结账、4充值、5会员创建通知）
     * @param type 操作类型
     * @return templateId
     */
    public static int getTemplateIdForMember(String type){
        switch (type){
            case "1":
                return 274763;
            case "2":
                return 274765;
            case "3":
                return 274767;
            case "4":
                return 274766;
            default:
                return 0;
        }
    }
}
