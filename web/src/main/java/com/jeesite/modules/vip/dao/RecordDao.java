/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.vip.entity.Record;

import java.util.HashMap;
import java.util.List;

/**
 * recordDAO接口
 * @author xtp
 * @version 2018-08-25
 */
@MyBatisDao
public interface RecordDao extends CrudDao<Record> {

    /**
     * 根据时间区间统计所有代理的总出货、入货、充值，积分等数据
     * @param  dateMap dateMap
     * @return hashMap
     */
    HashMap<String,Object> countAllAgentRecordByDate(HashMap<String,String> dateMap);

    /**
     * 根据时间区间和代理ID统计某个代理的总出货、入货、充值，积分等数据
     * @param  dateMap dateMap
     * @return hashMap
     */
    HashMap<String,Object> countAgentRecordByDateAndAgentId(HashMap<String,String> dateMap);

    /**
     * 根据时间区间和会员ID统计某个会员的总出货、入货、充值，积分等数据
     * @param  dateMap dateMap
     * @return hashMap
     */
    HashMap<String,Object> countMemberRecordByDateAndMemberId(HashMap<String,String> dateMap);
}