/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.entity;

import javax.validation.Valid;

import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.utils.excel.annotation.ExcelField;
import com.jeesite.common.utils.excel.annotation.ExcelField.Align;
import com.jeesite.common.utils.excel.annotation.ExcelFields;
import com.jeesite.modules.sys.entity.*;

/**
 * 员工用户管理Entity
 * @author ThinkGem
 * @version 2017-03-25
 */
@Table(name="${_prefix}sys_user", alias="a", columns={
        @Column(includeEntity=User.class),
    }, joinTable={
        @JoinTable(type=Type.JOIN, entity=Employee.class, alias="e",
                on="e.emp_code=a.ref_code AND a.user_type=#{USER_TYPE_EMPLOYEE}",
                attrName="employee", columns={
                @Column(includeEntity=Employee.class)
        }),
    },orderBy="a.update_date DESC"
)
public class VipUser extends User {

    private static final long serialVersionUID = 1L;

    /**代理用户拥有的会员数量**/
    private long vipCount;

    /**用户角色名称**/
    private String roleName;

    public VipUser() {
        this(null);
    }

    public VipUser(String id){
        super(id);
    }

    public long getVipCount() {
        return vipCount;
    }

    private Double agentBalance;

    public void setVipCount(long vipCount) {
        this.vipCount = vipCount;
    }
    @Valid
    @ExcelFields({
            @ExcelField(title="登录账号", attrName="loginCode", align=Align.CENTER, sort=30),
            @ExcelField(title="用户昵称", attrName="userName", align=Align.CENTER, sort=40),
            @ExcelField(title="电子邮箱", attrName="email", align=Align.LEFT, sort=50),
            @ExcelField(title="手机号码", attrName="mobile", align=Align.CENTER, sort=60),
            @ExcelField(title="办公电话", attrName="phone", align=Align.CENTER, sort=70),
            @ExcelField(title="员工编码", attrName="employee.empCode", align=Align.CENTER, sort=80),
            @ExcelField(title="员工姓名", attrName="employee.empName", align=Align.CENTER, sort=95),
            @ExcelField(title="拥有角色编号", attrName="userRoleString", align=Align.LEFT, sort=800, type=ExcelField.Type.IMPORT),
            @ExcelField(title="最后登录日期", attrName="lastLoginDate", align=Align.CENTER, sort=900, type=ExcelField.Type.EXPORT, dataFormat="yyyy-MM-dd HH:mm"),
    })
    public Employee getEmployee(){
        Employee employee = (Employee)super.getRefObj();
        if (employee == null){
            employee = new Employee();
            super.setRefObj(employee);
        }
        return employee;
    }

    public void setEmployee(Employee employee){
        super.setRefObj(employee);
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Double getAgentBalance() {
        return agentBalance;
    }

    public void setAgentBalance(Double agentBalance) {
        this.agentBalance = agentBalance;
    }
}