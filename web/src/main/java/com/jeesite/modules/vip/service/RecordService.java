/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.service;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.vip.entity.Record;
import com.jeesite.modules.vip.dao.RecordDao;

import java.util.HashMap;
import java.util.List;

/**
 * recordService
 * @author xtp
 * @version 2018-08-25
 */
@Service
@Transactional(readOnly=true)
public class RecordService extends CrudService<RecordDao, Record> {
	
	/**
	 * 获取单条数据
	 * @param record
	 * @return
	 */
	@Override
	public Record get(Record record) {
		return super.get(record);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param record
	 * @return
	 */
	@Override
	public Page<Record> findPage(Page<Record> page, Record record) {
		return super.findPage(page, record);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param record
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(Record record) {
		super.save(record);
	}
	
	/**
	 * 更新状态
	 * @param record
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Record record) {
		super.updateStatus(record);
	}
	
	/**
	 * 删除数据
	 * @param record
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Record record) {
		super.delete(record);
	}


	/**
	 * 根据时间区间统计所有代理的总出货、入货、充值，积分等数据
	 * @return hashMap
	 */
	public HashMap<String,Object> countAllAgentRecordByDate(String startTime , String endTime){
		HashMap<String,String> dateMap = new HashMap<>();
		dateMap.put("startTime",startTime);
		dateMap.put("endTime",endTime);
		return dao.countAllAgentRecordByDate(dateMap);
	}

	/**
	 * 根据时间区间统计所有代理的总出货、入货、充值，积分等数据
	 * @return hashMap
	 */
	public HashMap<String,Object> countAgentRecordByDateAndAgentId(String startTime , String endTime ,String agentId){
		HashMap<String,String> dateMap = new HashMap<>();
		dateMap.put("startTime",startTime);
		dateMap.put("endTime",endTime);
		dateMap.put("agentId",agentId);
		return dao.countAgentRecordByDateAndAgentId(dateMap);
	}

	/**
	 * 根据时间区间统计某会员的总出货、入货、充值，积分等数据
	 * @return hashMap
	 */
	public HashMap<String,Object> countMemberRecordByDateAndMemberId(String startTime , String endTime ,String memberId){
		HashMap<String,String> dateMap = new HashMap<>();
		dateMap.put("startTime",startTime);
		dateMap.put("endTime",endTime);
		dateMap.put("memberId",memberId);
		return dao.countMemberRecordByDateAndMemberId(dateMap);
	}
	
}