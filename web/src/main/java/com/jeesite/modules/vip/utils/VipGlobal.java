package com.jeesite.modules.vip.utils;

import com.jeesite.modules.sys.utils.UserUtils;
import com.jeesite.modules.vip.state.VipUserTypeEnum;

/**
 *
 * @author apple
 */
public class VipGlobal {

    /**
     * 判断当前用户是否是会员系统的管理员用户
     * @return true or false
     */
    public static boolean nowUserIsVipAdminUser(){
        return  VipUserTypeEnum.ADMIN.getValue().equals(UserUtils.getUser().getExtend().getExtendS1())|| UserUtils.getUser().isSuperAdmin();
    }

    /**
     * 判断当前用户是否是只读角色
     * @return true or false
     */
    public static boolean nowUserIsReadOnlyUser(){
        return  VipUserTypeEnum.READONLY_USER.getValue().equals(UserUtils.getUser().getExtend().getExtendS1());
    }
}
