package com.jeesite.modules.vip.task;

import com.jeesite.modules.sys.utils.LogUtils;
import com.jeesite.modules.vip.dao.TaskDao;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;



@Component
public class TaskUtil {

    private static Logger logger = Logger.getLogger(TaskUtil.class);

    @Autowired
    private TaskDao taskDao;

    @Scheduled(cron = "0 0 5 * * ?")
    public void deleteLogsAndRecords() throws Exception {
       logger.info("定时任务-删除日志和记录开始");
       taskDao.deleteLogs();
       taskDao.deleteRecords();
       logger.info("定时任务-删除日志和记录結束");
    }

}
