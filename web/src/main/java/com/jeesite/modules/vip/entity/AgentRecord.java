package com.jeesite.modules.vip.entity;

import com.jeesite.common.entity.Page;
import com.jeesite.common.utils.excel.annotation.ExcelField;
import com.jeesite.common.utils.excel.annotation.ExcelFields;
import com.jeesite.common.utils.excel.fieldtype.CompanyType;
import com.jeesite.common.utils.excel.fieldtype.OfficeType;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import java.util.Date;

public class AgentRecord {
    //代理用户code
    private String userCode;
    //代理账号
    private String loginName;
    //代理名字
    private String name;
    //出货
    private Double out;
    //入货
    private Double in;
    //积分
    private Double points;
    //总充值
    private Double rechargeTotal;
    //总支出
    private Double payTotal;
    //起始时间
    private Date startTime;
    //结束时间
    private Date endTime;

    //分页
    private Page page;

    @Valid
    @ExcelFields({
            @ExcelField(title="账号", attrName="loginName", align= ExcelField.Align.CENTER, sort=10),
            @ExcelField(title="代理名称", attrName="name", align = ExcelField.Align.CENTER, sort=20),
            @ExcelField(title="出货", attrName="out", align= ExcelField.Align.CENTER, sort=30),
            @ExcelField(title="入货", attrName="in", align= ExcelField.Align.CENTER, sort=40),
            @ExcelField(title="积分", attrName="points", align= ExcelField.Align.LEFT, sort=50),
            @ExcelField(title="总充值", attrName="rechargeTotal", align= ExcelField.Align.CENTER, sort=60),
            @ExcelField(title="总支出", attrName="payTotal", align= ExcelField.Align.CENTER, sort=60),
    })

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getOut() {
        return out;
    }

    public void setOut(Double out) {
        this.out = out;
    }

    public Double getIn() {
        return in;
    }

    public void setIn(Double in) {
        this.in = in;
    }

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }

    public Double getRechargeTotal() {
        return rechargeTotal;
    }

    public void setRechargeTotal(Double rechargeTotal) {
        this.rechargeTotal = rechargeTotal;
    }

    public Double getPayTotal() {
        return payTotal;
    }

    public void setPayTotal(Double payTotal) {
        this.payTotal = payTotal;
    }

    @NotBlank(message="开始时间不能为空")
    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }
}
