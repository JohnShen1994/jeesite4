package com.jeesite.modules.vip.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mac
 * @date 16/4/21
 */
public enum ReviewerEnum {
    /**
     *审核状态（0未审核，1审核通过，2审核不通过）
     */
    UN_CHECK(0, "0"),
    PASS(1, "1"),
    REJECT(2, "2");

    private Integer key;
    private String value;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private ReviewerEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String getValue(int index) {
        for (ReviewerEnum item : ReviewerEnum.values()) {
            if (item.getKey() == index) {
                return item.value;
            }
        }
        return null;
    }

    // 把 enum 转为 list 使用
    public static List<Map> getList() {
        List<Map> list = new ArrayList();
        Map map = null;
        for (ReviewerEnum item : ReviewerEnum.values()) {
            map = new HashMap();
            map.put("key", item.getKey());
            map.put("value", item.getValue());

            list.add(map);
        }
        return list;
    }
}
