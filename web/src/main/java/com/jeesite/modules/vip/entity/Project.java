/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.entity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * projectEntity
 * @author xtp
 * @version 2018-08-22
 */
@Table(name="vip_project", alias="a", columns={
		@Column(name="id", attrName="id", label="项目id", isPK=true),
		@Column(name="project_code", attrName="projectCode", label="项目编码"),
		@Column(name="name", attrName="name", label="项目名称", queryType=QueryType.LIKE),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.update_date DESC"
)
public class Project extends DataEntity<Project> {
	
	private static final long serialVersionUID = 1L;
	private String projectCode;		// 项目编码
	private String name;		// 项目名称
	
	public Project() {
		this(null);
	}

	public Project(String id){
		super(id);
	}
	
	@Length(min=0, max=32, message="项目编码长度不能超过 32 个字符")
	public String getProjectCode() {
		return projectCode;
	}

	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	
	@NotBlank(message="项目名称不能为空")
	@Length(min=0, max=64, message="项目名称长度不能超过 64 个字符")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}