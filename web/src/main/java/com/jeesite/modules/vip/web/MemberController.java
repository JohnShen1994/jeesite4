/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeesite.common.lang.StringUtils;
import com.jeesite.common.msg.SmsUtils;
import com.jeesite.modules.sys.entity.User;
import com.jeesite.modules.sys.service.UserService;
import com.jeesite.modules.sys.utils.UserUtils;
import com.jeesite.modules.vip.service.VipUserService;
import com.jeesite.modules.vip.state.VipUserTypeEnum;
import com.jeesite.modules.vip.utils.SMSUtils;
import com.jeesite.modules.vip.utils.VipGlobal;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.vip.entity.Member;
import com.jeesite.modules.vip.service.MemberService;

import java.util.HashMap;
import java.util.List;

/**
 * memberController
 * @author xtp
 * @version 2018-08-22
 */
@Controller
@RequestMapping(value = "${adminPath}/vip/member")
public class MemberController extends BaseController {

	@Autowired
	private MemberService memberService;
	@Autowired
	private VipUserService vipUserService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Member get(String id, boolean isNewRecord) {
		return memberService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("vip:member:view")
	@RequestMapping(value = {"list", ""})
	public String list(Member member, Model model) {
		//获取代理
		member.setStatus(StringUtils.isNotBlank(member.getStatus()) ? member.getStatus() : User.STATUS_NORMAL);
		model.addAttribute("agentList", vipUserService.findAgentList(""));
		model.addAttribute("member", member);
		if(UserUtils.getCache("deviceType") != null && "mobile".equals(UserUtils.getCache("deviceType"))){
			return "modules/mobile/memberList";
		}
		return "modules/vip/memberList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("vip:member:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Member> listData(Member member, HttpServletRequest request, HttpServletResponse response) {
		if(!VipGlobal.nowUserIsVipAdminUser() && !VipGlobal.nowUserIsReadOnlyUser()){
			String agentId = StringUtils.isNotBlank(member.getAgentId()) ? member.getAgentId() : UserUtils.getUser().getUserCode();
			member.setAgentId(agentId);
		}
		Page<Member> page = memberService.findPage(new Page<Member>(request, response), member);
		//获取统计数据
		HashMap<String,Object> countMap = memberService.countMemberListData(member.getAgentId(),member.getStatus());
		page.setOtherData(countMap);
		return page;
	}

	/**
	 * 查看编辑会员
	 */
	@RequiresPermissions("vip:member:view")
	@RequestMapping(value = "form")
	public String form(Member member, Model model) {
		User user = UserUtils.getUser();
		String userCode = VipGlobal.nowUserIsVipAdminUser() ? "" : user.getUserCode();
		model.addAttribute("agentList", vipUserService.findAgentList(userCode));
		model.addAttribute("member", member);
		return "modules/vip/memberForm";
	}

	/**
	 * 保存member
	 */
	@RequiresPermissions("vip:member:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Member member) {
		if(member.getIsNewRecord()){
			member.setBalance(0.00D);
			member.setPoints(0.00D);
			member.setRechargeTotal(0.00D);
			SMSUtils.sendingNotice(SMSUtils.getTemplateId("5"),member.getMobile(),member.getName());
		}
		memberService.save(member);
		return renderResult(com.jeesite.common.config.Global.TRUE, text("保存成功！"));
	}
	
	/**
	 * 停用member
	 */
	@RequiresPermissions("vip:member:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(Member member) {
		member.setStatus(Member.STATUS_DISABLE);
		memberService.updateStatus(member);
		return renderResult(com.jeesite.common.config.Global.TRUE, text("停用会员成功"));
	}
	
	/**
	 * 启用member
	 */
	@RequiresPermissions("vip:member:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(Member member) {
		member.setStatus(Member.STATUS_NORMAL);
		memberService.updateStatus(member);
		return renderResult(com.jeesite.common.config.Global.TRUE, text("启用会员成功"));
	}
	
	/**
	 * 删除member
	 */
	@RequiresPermissions("vip:member:del")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Member member) {
		if(User.STATUS_NORMAL.equals(member.getStatus())){
			return renderResult(com.jeesite.common.config.Global.FALSE, text("删除会员失败，需要停用会员才能删除用户!"));
		}
		memberService.delete(member);
		return renderResult(com.jeesite.common.config.Global.TRUE, text("删除会员成功！"));
	}
}