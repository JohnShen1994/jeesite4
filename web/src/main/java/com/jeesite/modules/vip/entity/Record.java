/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.entity;

import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.utils.excel.annotation.ExcelField;
import com.jeesite.common.utils.excel.annotation.ExcelFields;
import com.jeesite.modules.sys.entity.User;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

import java.util.Date;

/**
 * recordEntity
 * @author xtp
 * @version 2018-08-25
 */
@Table(name="vip_record", alias="a", columns={
		@Column(name="id", attrName="id", label="流水id", isPK=true),
		@Column(name="type", attrName="type", label="类型"),
		@Column(name="money", attrName="money", label="金额"),
		@Column(name="balance", attrName="balance", label="会员余额"),
		@Column(name="member_id", attrName="memberId", label="会员"),
		@Column(name="reviewer_state", attrName="reviewerState", label="审核状态"),
		@Column(name="reviewer_id", attrName="reviewerId", label="审核人"),
		@Column(name="project_id", attrName="projectId", label="项目"),
		@Column(name="pay_type", attrName="payType", label="支付方式"),
		@Column(includeEntity=DataEntity.class),
	},
		joinTable = {
				@JoinTable(type = JoinTable.Type.LEFT_JOIN,entity = Member.class,alias = "m",
						on="a.member_id = m.id",columns = {@Column(includeEntity = Member.class)}),

				@JoinTable(type = JoinTable.Type.LEFT_JOIN,entity = Project.class,alias = "p",
						on="a.project_id = p.id",columns = {@Column(includeEntity = Project.class)}),

				@JoinTable(type = JoinTable.Type.LEFT_JOIN,entity = User.class,alias = "sys",
						on="sys.user_code = m.agent_id",attrName="agentUser" ,columns = {
						@Column(name="user_code", label="用户编码", isPK=true),
						@Column(name="user_name", label="用户名称")}),
				@JoinTable(type = JoinTable.Type.LEFT_JOIN,entity = User.class,alias = "ur",
						on="ur.user_code = a.reviewer_id",attrName="reviewer" ,columns = {
						@Column(name="user_name", label="用户名称")})
		},orderBy="a.update_date DESC"
)
public class Record extends DataEntity<Record> {
	
	private static final long serialVersionUID = 1L;

	/**操作类型**/
	private String type;

	/**操作类型1（用于查询）**/
	private String type1;

	/**操作金额**/
	private Double money;

	/**当前会员余额**/
	private Double balance;

	private String memberId;

	/**审核状态**/
	private String reviewerState;

	/**审核人id**/
	private String reviewerId;

	/**审核人**/
	private User reviewer;

	/**项目id**/
	private String projectId;

	/**支付方式**/
	private String payType;

	private Project project;
	private User agentUser;
	private Member member;
	private Date startTime;
	private Date endTime;

	public Record() {
		this(null);
	}

	public Record(String id){
		super(id);
	}

	@Valid
	@ExcelFields({
			@ExcelField(title="类型", attrName="type", align= ExcelField.Align.CENTER, sort=10),
			@ExcelField(title="金额", attrName="money", align = ExcelField.Align.CENTER, sort=20),
			@ExcelField(title="会员", attrName="member.name", align= ExcelField.Align.CENTER, sort=30,fieldType=Member.class),
			@ExcelField(title="审核状态", attrName="reviewerState", align= ExcelField.Align.CENTER, sort=40),
			@ExcelField(title="项目", attrName="project.name", align= ExcelField.Align.LEFT, sort=50,fieldType=Project.class),
	})
	
	@NotBlank(message="类型不能为空")
	@Length(min=0, max=2, message="类型长度不能超过 2 个字符")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType1() {
		return type1;
	}

	public void setType1(String type1) {
		this.type1 = type1;
	}

	@NotNull(message="金额不能为空")
	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	@NotBlank(message="会员不能为空")
	@Length(min=0, max=32, message="会员长度不能超过 32 个字符")
	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	@Length(min=0, max=2, message="审核状态长度不能超过 2 个字符")
	public String getReviewerState() {
		return reviewerState;
	}

	public void setReviewerState(String reviewerState) {
		this.reviewerState = reviewerState;
	}
	
	@Length(min=0, max=64, message="审核人长度不能超过 64 个字符")
	public String getReviewerId() {
		return reviewerId;
	}

	public void setReviewerId(String reviewerId) {
		this.reviewerId = reviewerId;
	}

	public User getReviewer() {
		return reviewer;
	}

	public void setReviewer(User reviewer) {
		this.reviewer = reviewer;
	}

	@Length(min=0, max=64, message="项目长度不能超过 64 个字符")
	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public User getAgentUser() {
		return agentUser;
	}

	public void setAgentUser(User agentUser) {
		this.agentUser = agentUser;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
}