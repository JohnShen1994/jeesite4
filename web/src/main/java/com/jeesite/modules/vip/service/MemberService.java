/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.vip.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jeesite.common.lang.StringUtils;
import com.jeesite.modules.sys.entity.User;
import com.jeesite.modules.vip.entity.AgentRecord;
import com.jeesite.modules.vip.entity.MemberRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.vip.entity.Member;
import com.jeesite.modules.vip.dao.MemberDao;

/**
 * memberService
 * @author xtp
 * @version 2018-08-22
 */
@Service
@Transactional(readOnly=true)
public class MemberService extends CrudService<MemberDao, Member> {

	@Autowired
	private MemberDao memberDao;

	/**
	 * 获取单条数据
	 * @param member
	 * @return
	 */
	@Override
	public Member get(Member member) {
		return super.get(member);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param member
	 * @return
	 */
	@Override
	public Page<Member> findPage(Page<Member> page, Member member) {
		return super.findPage(page, member);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param member
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Member member) {
		super.save(member);
	}
	
	/**
	 * 更新状态
	 * @param member
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Member member) {
		super.updateStatus(member);
	}
	
	/**
	 * 删除数据
	 * @param member
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Member member) {
		super.delete(member);
	}

	/**
	 * 代理流水账分页
	 * @param map
	 * @return
	 */
    public Page findAgentRecordPage(Map map ,Page page) {
		//查询代理列表
		map.put("pageSize",page.getPageSize());
		map.put("size",(page.getPageNo() - 1) * page.getPageSize());
		List<AgentRecord> list= memberDao.findAgentRecordPage(map);
		page.setList(list);
		//查询统计总条数
		page.setCount(memberDao.findAgentRecordPageCount(map));
		return page;
    }

	/**
	 * 会员流水账分页
	 * @param map
	 * @return
	 */
	public Page findMemberRecordPage(Map map,Page page) {
		//查询代理列表
		map.put("pageSize",page.getPageSize());
		map.put("size",(page.getPageNo() - 1) * page.getPageSize());
		List<MemberRecord> list= memberDao.findMemberRecordPage(map);
		page.setList(list);
		//查询统计总条数
		page.setCount(memberDao.findMemberRecordPageCount(map));
		return page;
	}

	/**
	 * 统计会员数据
	 * @param agentId agentId
	 * @return hashMap
	 */
	public HashMap<String,Object> countMemberListData(String agentId ,String status){
		HashMap<String,Object> map = new HashMap<>();
		map.put("agentId",agentId);
		map.put("status",status);
		return dao.countMemberListData(map);
	}
}